# CODEOWNERS
This project has simple CODEOWNERS configuration

## Requirements

- CODEOWNERS file in any of the below locations
    - Root directory of the repository
    - `.gitlab/` directory
    - `docs/` directory 
- The user or group are not a member of the project or parent group. 
- Set up [Code Owner approval on a protected branch](https://docs.gitlab.com/ee/user/project/protected_branches.html#require-code-owner-approval-on-a-protected-branch). 

## Docs
[Code owners](https://docs.gitlab.com/ee/user/project/code_owners.html)
